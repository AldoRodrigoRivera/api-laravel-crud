<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Bienvenido a mi API';
    }

    
    public function indexAll($token)
    {   
        $result =  Products::all();
        return $result;
        // return $token;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $token = $request->token;
        
        $data = $this->validate(request(), [
            'name' => 'required',
            'amount' => 'required|numeric',
        ]);

        if($token == "Rica1994"){

            $product = new Products();
            $product->name  = "$request->name";
            $product->amount = "$request->amount";
            $product->save();

            return $product;
        }else{
            return "Acceso no Autorizado, TOKEN inválido";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $products)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
    }
}
